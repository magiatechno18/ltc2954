/*
 * LTC2954 driver
 *
 * Copyright (C) 2014, Xsens Technologies BV <info@xsens.com>
 * Modified by: Damien KIRK <damien.kirk@magia-diagnostics.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ----------------------------------------
 * - Description
 * ----------------------------------------
 *
 * This driver is to be used with an external PowerPath Controller (LTC2954).
 * Its function is to determine when a external shut down is triggered
 * and react by offering the information on a miscellaneous character device.
 *
 * This driver expects a device tree with a ltc2954 entry for pin mapping.
 *
 * ----------------------------------------
 * - GPIO - TODO check
 * ----------------------------------------
 *
 * The following GPIOs are used:
 * - trigger (input)
 *     A level change indicates the shut-down trigger. If it's state reverts
 *     within the time-out defined by trigger_delay, the shut down is not
 *     executed. If no pin is assigned to this input, the driver will start the
 *     watchdog toggle immediately. The chip will only power off the system if
 *     it is requested to do so through the kill line.
 *
 * - watchdog (output)
 *     Once a shut down is triggered, the driver will toggle this signal,
 *     with an internal (wde_interval) to stall the hardware shut down.
 *
 * - kill (output)
 *     The last action during shut down is triggering this signalling, such
 *     that the PowerPath Control will power down the hardware.
 *
 * ----------------------------------------
 * - Interrupts
 * ----------------------------------------
 *
 * The driver requires a non-shared, edge-triggered interrupt on the trigger
 * GPIO.
 *
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/ktime.h>
#include <linux/slab.h>
#include <linux/kmod.h>
#include <linux/module.h>
#include <linux/gpio/consumer.h>
#include <linux/reboot.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/workqueue.h>
#include <linux/poll.h>

#define MISC_NAME "LTC2954-poweroff"

static struct gpio_desc *ltc2954_gpio_kill = NULL;

/**
 * @brief The ltc2954_poweroff struct
 * @param gpio_int : low asserted input interrupt gpio. Requests the µC to gracefully poweroff.
 * @param gpio_kill : low asserted output kill gpio. Will turn the power supply off.
 * @param shutdown_request : boolean holding the value of gpio_int during the last interrupt.
 * @param irq_enabled : flag set to true if the irq is enabled. In this case, the polling process can be put to sleep with a possibility of waking up. If the flag is set to false, polling should not go to sleep and should instead return immediately.
 * @param irq_triggered : flag set to false before the polling process is put to sleep. Should be set to true by the interrupt, and checked when exiting sleep.
 */
struct ltc2954_poweroff {
    struct device *dev;
    struct gpio_desc *gpio_int;
    struct gpio_desc *gpio_kill;

    /* Work structure in which the bottom half of the interrupt will be scheduled */
    struct workqueue_struct *ltc2954_wq;
    struct work_struct work;

    /* Queue head in which the polling process will be put to sleep */
    wait_queue_head_t is_data_ready_queue; /* see hv_utils_transport */

    bool irq_enabled;
    bool shutdown_request;
    bool irq_triggered;

    struct miscdevice misc_device;
};

#define to_ltc2954(p, m) container_of(p, struct ltc2954_poweroff, m);

static int ltc2954_fopen(struct inode* node, struct file* file) {
    printk(KERN_INFO "Device file opened\n");
    return 0;
}

static int ltc2954_frelease(struct inode* node, struct file* file) {
    printk(KERN_INFO "Device file closed \n");
    return 0;
}

static ssize_t ltc2954_fwrite(struct file* file, const char *buf, size_t size, loff_t* off) {
    struct ltc2954_poweroff *chip = to_ltc2954(file->private_data, misc_device);
    dev_info(chip->dev, "Write called\n");

    unsigned char value[10];
    char *reset_cmd = "reset";

    if (copy_from_user(value, buf, size)) {
        return -EFAULT;
    }

    /* When "reset" is written to the char dev, the driver's register should be reset,
     * as if the interrupt never had triggered and stored the GPIO data in chip->shutdown_request. */
    if (strncmp(reset_cmd, buf, size) == 0) {
        dev_info(chip->dev, "Reset called\n");
        chip->irq_triggered = false;
        chip->shutdown_request = false;
        return 0;
    }

    return -EINVAL;

}

static int ltc2954_fread(struct file* file, char __user *buf, size_t size, loff_t* off) {
    struct ltc2954_poweroff *chip = to_ltc2954(file->private_data, misc_device);
    dev_info(chip->dev, "Read called\n");

    char ch[2];
    ch[1] = '\n';
    bool value = gpiod_get_value(chip->gpio_int);

    /* If the IRQ was triggered, the value of the GPIO at the moment of the trigger
     * is stored in chip->shutdown_request.
     * If the IRQ was not triggered, we return the current value of the GPIO. */

    if (chip->irq_triggered) {
        ch[0] = chip->shutdown_request ? '1' : '0';
    } else {
        ch[0] = gpiod_get_value(chip->gpio_int) ? '1' : '0';
    }

    if (copy_to_user(buf, &ch, 2)) {
        return -EFAULT;
    }
    return 2;
}

static unsigned int ltc2954_fpoll(struct file* file, poll_table *wait) {
    struct ltc2954_poweroff *chip = to_ltc2954(file->private_data, misc_device);
    unsigned int mask = 0;

    /* If the IRQ is disabled, nothing can wake the poll up except timeout. In this case
     * it is preferable not to use the wait at all, and return that the device can be read.
     * It will read the real-time value of the GPIO instead of the one stored in the interrupt. */
    if (!chip->irq_enabled) {
        chip->irq_triggered = false;
        return POLLIN | POLLRDNORM;
    }

    chip->irq_triggered = false;
    poll_wait(file, &chip->is_data_ready_queue, wait);
    if (chip->irq_triggered) {
        return POLLIN | POLLRDNORM;
    } else {
        return POLLERR | POLLHUP;
    }
}

static struct file_operations ltc2954_fops = {
    .owner = THIS_MODULE,
    .read = ltc2954_fread,
    .write = ltc2954_fwrite,
    .open = ltc2954_fopen,
    .release = ltc2954_frelease,
};

static void ltc2954_poweroff_kill(void) {
    gpiod_set_value(ltc2954_gpio_kill, 1);
}

static void ltc2954_poweroff_irq_bottom(struct work_struct *work_s) {
    struct ltc2954_poweroff *chip = to_ltc2954(work_s, work);
    chip->irq_triggered = true;
    wake_up_interruptible(&chip->is_data_ready_queue);
}

static irqreturn_t ltc2954_poweroff_irq_handler(int irq, void *dev_id) {
    printk("LTC2954 interrupt\n");
    struct ltc2954_poweroff *data = dev_id;
    dev_info(data->dev, "LTC2954 interrupt received\n");

    /* We're storing the value of the gpio right now, as even if it is supposed
     * to stay active for 32ms, we aren't sure when the next method is going to run. */
    data->shutdown_request = gpiod_get_value(data->gpio_int);

    /* We could either wake up the polling process now, or delay this wake up
     * to the bottom half of the interrupt. For the sake of learning, it will be
     * delayed to the bottom half that is queued with the following statement. */
    queue_work(data->ltc2954_wq, &data->work);
    return IRQ_HANDLED;
}

/**
 * @brief ltc2954_poweroff_init
 * @param pdev
 * @return
 */
static int ltc2954_poweroff_init(struct platform_device *pdev) {
    int ret;
    struct ltc2954_poweroff *data = platform_get_drvdata(pdev);

    data->shutdown_request = false;
    data->irq_triggered = false;

    data->gpio_int = devm_gpiod_get(&pdev->dev, "pdnreq", GPIOD_IN);
    if (IS_ERR(data->gpio_int)) {
        ret = PTR_ERR(data->gpio_int);
        dev_err(&pdev->dev, "Unable to claim gpio \"int\"\n");
        return ret;
    }

    /* The kill signal should not be asserted */
    data->gpio_kill = devm_gpiod_get(&pdev->dev, "kill", GPIOD_OUT_LOW);
    if (IS_ERR(data->gpio_kill)) {
        ret = PTR_ERR(data->gpio_kill);
        dev_err(&pdev->dev, "Unable to claim gpio \"kill\"\n");
        return ret;
    }

    /* Setting the static ltc2954_gpio_kill to the struct gpio_desc allocated previously. */
    ltc2954_gpio_kill = data->gpio_kill;

    if(devm_request_irq(&pdev->dev, gpiod_to_irq(data->gpio_int),
                        ltc2954_poweroff_irq_handler,
                        (IRQF_TRIGGER_RISING | IRQF_TRIGGER_FALLING),
                        "ltc2954-poweroff-irq",
                        data)) {
        dev_warn(&pdev->dev, "Unable to configure the shutdown interrupt\n");
        data->irq_enabled = false;
    }
    data->irq_enabled = true;

    /* Initializing the queue in which the polling process will be put to sleep */
    init_waitqueue_head(&data->is_data_ready_queue);

    return 0;
}

static int ltc2954_poweroff_probe(struct platform_device *pdev) {
    int err;
    struct ltc2954_poweroff *ltc2954_chip;

    dev_info(&pdev->dev, "Probing LTC2954 driver\n");
    ltc2954_chip = devm_kzalloc(&pdev->dev, sizeof(*ltc2954_chip), GFP_KERNEL);
    if (!ltc2954_chip) {
        dev_err(&pdev->dev, "%s: Could not allocate memory\n", __func__);
        return -ENOMEM;
    }

    /* Preparing the work queue and work structure handling the bottom half of the IRQ.
     * To schedule the execution of ltc2954_poweroff_irq_bottom, the work structure has
     * to be put in the queue. */
    ltc2954_chip->ltc2954_wq = alloc_workqueue("ltc2954_wq", 0, 0);
    INIT_WORK(&ltc2954_chip->work, ltc2954_poweroff_irq_bottom);

    ltc2954_chip->dev = &pdev->dev;
    platform_set_drvdata(pdev, ltc2954_chip);

    /* Setting up the miscellaneous character device. */
    ltc2954_chip->misc_device.minor = MISC_DYNAMIC_MINOR;
    ltc2954_chip->misc_device.name = MISC_NAME;
    ltc2954_chip->misc_device.fops = &ltc2954_fops;

    err = misc_register(&ltc2954_chip->misc_device);
    if (err < 0) {
        dev_err(&pdev->dev, "%s: Could not register to misc framework\n", __func__);
        return err;
    }

    dev_info(&pdev->dev, "LTC2954 set up, minor number %i\n", ltc2954_chip->misc_device.minor);

    /* Reading the device tree to get and initialize the gpios. */
    err = ltc2954_poweroff_init(pdev);
    if (err < 0) {
        dev_err(&pdev->dev, "%s: COuld not initialize ltc2954\n", __func__);
        goto out_misc_dereg;
    }

    /* Setting up the power management callback. */
    dev_info(&pdev->dev, "Setting up the power management callback\n");
    if (pm_power_off) {
        dev_err(&pdev->dev, "pm_power_off already registered\n");
        err = -EBUSY;
        goto out_misc_dereg;
    }
    pm_power_off = ltc2954_poweroff_kill;

    return 0;

out_misc_dereg:
    misc_deregister(&ltc2954_chip->misc_device);
    dev_info(&pdev->dev, "Deregistered misc device\n");
    return err;
}

static int ltc2954_poweroff_remove(struct platform_device *pdev) {
    struct ltc2954_poweroff *chip = platform_get_drvdata(pdev);
    flush_workqueue(chip->ltc2954_wq);
    destroy_workqueue(chip->ltc2954_wq);

    if (ltc2954_poweroff_kill == pm_power_off) {
        dev_info(&pdev->dev, "Unregistering pm_power_off\n");
        pm_power_off = NULL;
        ltc2954_gpio_kill = NULL;
    }

    misc_deregister(&chip->misc_device);
    return 0;
}

static const struct of_device_id ltc2954_dt_ids[] = {
    { .compatible = "ltc,ltc2954" },
    { /* sentinel */ },
};
MODULE_DEVICE_TABLE(of, ltc2954_dt_ids);

static struct platform_driver ltc2954_poweroff_driver = {
    .probe = ltc2954_poweroff_probe,
    .remove = ltc2954_poweroff_remove,
    .driver = {
        .name = "ltc2954-poweroff",
        .of_match_table = ltc2954_dt_ids,
    },
};

module_platform_driver(ltc2954_poweroff_driver);

MODULE_AUTHOR("Damien KIRK <damien.kirk@magia-diagnostics.com>");
MODULE_DESCRIPTION("LTC power-off driver");
MODULE_LICENSE("GPL v2");
